<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <!-- <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">Form </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Form Update Item</h4>
                    </div>
                </div>
            </div> -->
            <!-- end page title -->

            <div class="row">
                <h4 class="page-title mt-2">Form Update Item</h4>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Scan QR-Code</h4>
                            <!-- SCANNER -->
                            <!-- <div id="scanner"></div> -->

                            <div style="text-align:center;">
                                <video id="previewKamera" style="width: 180px;height: 180px;"></video>
                                <br>
                                <select id="pilihKamera" style="max-width:200px"></select>
                            </div>
                            <script src="<?php echo base_url('assets/js'); ?>/scan-qr.min.js"></script>

                            <script>
                                let selectedDeviceId = null;
                                const codeReader = new ZXing.BrowserMultiFormatReader();
                                const sourceSelect = document.getElementById('pilihKamera');
                                const videoElement = document.createElement('video');
                                const scannerDiv = document.getElementById('scanner');

                                function handleCameraChange() {
                                    selectedDeviceId = sourceSelect.value;
                                    if (codeReader) {
                                        codeReader.reset();
                                        initScanner();
                                    }
                                }

                                sourceSelect.addEventListener('change', handleCameraChange);

                                function initScanner() {
                                    codeReader
                                        .listVideoInputDevices()
                                        .then(videoInputDevices => {
                                            videoInputDevices.forEach(device =>
                                                console.log(`${device.label}, ${device.deviceId}`)
                                            );

                                            if (videoInputDevices.length > 0) {
                                                if (selectedDeviceId == null) {
                                                    if (videoInputDevices.length > 1) {
                                                        selectedDeviceId = videoInputDevices[1].deviceId;
                                                    } else {
                                                        selectedDeviceId = videoInputDevices[0].deviceId;
                                                    }
                                                }

                                                if (videoInputDevices.length >= 1) {
                                                    sourceSelect.innerHTML = '';
                                                    videoInputDevices.forEach(element => {
                                                        const sourceOption = document.createElement('option');
                                                        sourceOption.text = element.label;
                                                        sourceOption.value = element.deviceId;
                                                        if (element.deviceId == selectedDeviceId) {
                                                            sourceOption.selected = true;
                                                        }
                                                        sourceSelect.appendChild(sourceOption);
                                                    });
                                                }

                                                codeReader
                                                    .decodeFromVideoDevice(selectedDeviceId, 'previewKamera', (result, error) => {
                                                        if (result) {
                                                            console.log(result.text);
                                                            const url = "https://portal2.incoe.astra.co.id/e-wip/api/getBarcodeId/" + result.text;
                                                            sendGetRequest(url);
                                                        }
                                                        if (error && !(error instanceof ZXing.NotFoundException)) {
                                                            console.error(error);
                                                        }
                                                    })
                                                    .then(() => {
                                                        console.log('Pemindaian selesai.');
                                                    })
                                                    .catch(err => {
                                                        console.error(err);
                                                    });

                                                videoElement.srcObject = null;
                                                videoElement.src = '';
                                                videoElement.controls = false;
                                                videoElement.autoplay = true;
                                                videoElement.muted = true;
                                                scannerDiv.appendChild(videoElement);

                                                codeReader
                                                    .attachVideoElement(videoElement)
                                                    .then(() => {
                                                        console.log('Kamera terhubung.');
                                                    })
                                                    .catch(err => {
                                                        console.error(err);
                                                    });
                                            } else {
                                                alert('Kamera tidak ditemukan!');
                                            }
                                        })
                                        .catch(err => {
                                            console.error(err);
                                        });
                                }

                                if (navigator.mediaDevices) {
                                    initScanner();
                                } else {
                                    alert('Tidak dapat mengakses kamera.');
                                }
                            </script>

                            <!-- <video id="preview" src="" class="d-flex justify-content-center" style="height: 150px;"></video> -->
                            <!-- SCANNER -->

                            <div class="mb-3">
                                <label for="code_qr" class="form-label">Code QR</label>
                                <input type="text" class="form-control" placeholder="Input QR-Code" id="qr" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>

                            <script type="text/javascript">
                                // variabel yang tidak berubah
                                const input = document.getElementById('qr');
                                // Membuat elemen <video> baru
                                // const videoElement = document.createElement('video');
                                // videoElement.style.height = '150px';

                                // // Mencari elemen <div> dengan id "scanner"
                                // const scannerDiv = document.getElementById('scanner');

                                // // Memasukkan elemen <video> ke dalam <div> "scanner"
                                // scannerDiv.appendChild(videoElement);

                                // // Menginisialisasi scanner dengan elemen <video> baru
                                // const scanner = new Instascan.Scanner({
                                //     video: videoElement
                                // });

                                // console.log(scanner);
                                // fungsi untuk menangani respons dari permintaan GET
                                function handleResponse() {
                                    if (this.readyState === 4 && this.status === 200) {
                                        const data = JSON.parse(this.responseText);
                                        if (data.results[0].NO_RFQ === null || data.results[0].NO_RFQ === "") {
                                            alert("Data kosong");
                                        } else {
                                            document.getElementById('no_rfq').value = data.results[0].NO_RFQ;
                                            document.getElementById('no_wo').value = data.results[0].NO_WO;
                                            document.getElementById('name_cust').value = data.results[0].CUSTOMER_NAME;
                                            document.getElementById('code_qr').value = data.results[0].NOTE;
                                            document.getElementById('qty').value = data.results[0].QTY;
                                            document.getElementById('warehouse').value = data.results[0].WAREHOUSE;
                                            document.getElementById('no_tag').value = data.results[0].NO_TAG;
                                            document.getElementById('name_item').value = data.results[0].ITEM;
                                            document.getElementById('desc_pn').value = data.results[0].DESCRIPTION_PN;
                                            document.getElementById('bpid').value = data.results[0].BPID;
                                            document.getElementById('no_sdf').value = data.results[0].SDF_ORDER;
                                            document.getElementById('lot_del').value = data.results[0].QTY_LOT_DELIV;
                                        }
                                    } else if (this.readyState === 4) {
                                        alert("Tidak ada koneksi");
                                    }
                                }

                                // fungsi untuk mengirimkan permintaan GET
                                function sendGetRequest(url) {
                                    const xhr = new XMLHttpRequest();
                                    xhr.open('GET', url, true);
                                    xhr.onreadystatechange = handleResponse;
                                    xhr.send();
                                }

                                // event listener untuk input
                                input.addEventListener('change', function(event) {
                                    const content = event.target.value;
                                    const url = "https://portal2.incoe.astra.co.id/e-wip/api/getBarcodeId/" + content;
                                    sendGetRequest(url);
                                });

                                // // event listener untuk scanner
                                // scanner.addListener('scan', function(content) {
                                //     const url = "https://portal2.incoe.astra.co.id/e-wip/api/getBarcodeId/" + content;
                                //     sendGetRequest(url);
                                // });

                                // // memulai scanner
                                // Instascan.Camera.getCameras().then(function(cameras) {
                                //     if (cameras.length > 0) {
                                //         scanner.start(cameras[0]);
                                //     } else {
                                //         console.error('camera tidak di temukan');
                                //     }
                                // }).catch(function(e) {
                                //     console.error(e);
                                // });
                            </script>

                            <!-- end scan qr from api -->

                            <!-- scan qr from database local -->
                            <!-- <script type="text/javascript">
                                let scanner = new Instascan.Scanner({
                                    video: document.getElementById('preview')
                                });
                                scanner.addListener('scan', function(content) {
                                    // menampilkan hasil dari scan qr code
                                    // $('#qrcode').val(content);
                                    alert(content);
                                    // membuat objek XMLHttpRequest
                                    let xhr = new XMLHttpRequest();

                                    // menentukan URL dari request
                                    let url = content;

                                    // menentukan method dan URL
                                    xhr.open('GET', url, true);

                                    // menambahkan event listener untuk menangani response
                                    xhr.onreadystatechange = function() {
                                        if (this.readyState === 4 && this.status === 200) {
                                            let data = JSON.parse(this.responseText);
                                            // menampilkan data di console
                                            document.getElementById('no_rfq').value = data.item.no_rfq;
                                            document.getElementById('no_wo').value = data.item.no_wo;
                                            document.getElementById('name_cust').value = data.item.name_cust;
                                            document.getElementById('code_qr').value = data.item.code_qr;
                                            document.getElementById('qty').value = data.item.qty;
                                            document.getElementById('warehouse').value = data.item.warehouse;
                                            document.getElementById('no_tag').value = data.item.no_tag;
                                            document.getElementById('name_item').value = data.item.name_item;
                                            document.getElementById('desc_pn').value = data.item.desc_pn;
                                            document.getElementById('bpid').value = data.item.bpid;
                                        }
                                    };

                                    xhr.send();
                                });
                                Instascan.Camera.getCameras().then(function(cameras) {
                                    if (cameras.length > 0) {
                                        scanner.start(cameras[0]);
                                    } else {
                                        console.error('camera tidak di temukan');
                                    }
                                }).catch(function(e) {
                                    console.error(e);
                                });
                            </script> -->
                            <!-- end scan qr from database local -->
                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Form Input Item</h4>

                            <form action="/admin/update/<?= $item['id']; ?>" method="post" class="needs-validation" novalidate>
                                <?= csrf_field(); ?>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="no_rfq" class="form-label">No RFQ</label>
                                            <input type="text" class="form-control" id="no_rfq" placeholder="No RFQ" name="no_rfq" autofocus value="<?= $item['no_rfq']; ?>" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="no_wo" class="form-label">No WO</label>
                                            <input type="text" class="form-control" id="no_wo" placeholder="No WO" name="no_wo" value="<?= $item['no_wo']; ?>" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="mb-3" hidden>
                                    <label for="name_cust" class="form-label">Name Customer</label>
                                    <input type="text" class="form-control" id="name_cust" placeholder="Name Customer" name="name_cust" value="<?= $item['name_cust']; ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="code_qr" class="form-label">Code QR</label>
                                    <input type="text" class="form-control" id="code_qr" placeholder="Code_QR" name="code_qr" value="<?= $item['code_qr']; ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="name_item" class="form-label">Name Item</label>
                                            <input type="text" class="form-control" id="name_item" placeholder="Name Item" name="name_item" value="<?= old('name_item'); ?>" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="mb-3" hidden>
                                    <label for="warehouse" class="form-label">Warehouse</label>
                                    <input type="text" class="form-control" id="warehouse" placeholder="Warehouse" name="warehouse" value="<?= old('warehouse'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="lot_del" class="form-label">Lot Delivery</label>
                                    <input type="text" class="form-control" id="lot_del" placeholder="Lot Devlivery" name="lot_del" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="no_sdf" class="form-label">No SDF</label>
                                    <input type="text" class="form-control" id="no_sdf" placeholder="No SDF" name="no_sdf" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="no_tag" class="form-label">No Tag</label>
                                    <input type="number" class="form-control" id="no_tag" placeholder="No_Tag" name="no_tag" value="<?= old('no_tag'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="desc_pn" class="form-label">Description PN</label>
                                    <input type="text" class="form-control" id="desc_pn" placeholder="Description PN" name="desc_pn" value="<?= old('desc_pn'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="bpid" class="form-label">BPID</label>
                                    <input type="text" class="form-control" id="bpid" placeholder="BPID" name="bpid" value="<?= old('bpid'); ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <!-- just show location detail -->
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="qty" class="form-label">QTY</label>
                                            <input type="number" class="form-control" id="qty" placeholder="QTY" name="qty" value="<?= $item['qty']; ?>" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="locationsDetail" class="form-label">Location</label>
                                            <input type="text" class="form-control" id="locationsDetail" placeholder="Locations Detail" value="<?= $item['locations']; ?>.<?= $item['sub_locations']; ?>" required readonly />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- end just show location datail -->
                                <div class="mb-3" hidden>
                                    <label for="rack" class="form-label">Rack</label>
                                    <input type="text" class="form-control" id="rack" placeholder="Rack" name="rack" value="<?= $item['rack']; ?>" required readonly />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="locations" class="form-label">Location</label>
                                    <input type="text" class="form-control" id="locations" placeholder="Locations" name="locations" value="<?= $item['locations']; ?>" required readonly />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="sub_locations" class="form-label">Sub Location</label>
                                    <input type="text" class="form-control" id="sub_locations" placeholder="Sub Locations" name="sub_locations" value="<?= $item['sub_locations']; ?>" required readonly />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->

            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


    <?= $this->endSection(); ?>