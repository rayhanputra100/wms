<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>

<div class="content-page">
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item active">Form </li>
                            </ol>
                        </div>
                        <h4 class="page-title">Form Update Item</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Form Input Item Receh</h4>
                            <p class="sub-header">This form for input item</p>

                            <form action="/admin/tambah" method="post" class="needs-validation" novalidate>
                                <?= csrf_field(); ?>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="no_rfq" class="form-label">No RFQ</label>
                                            <input type="text" class="form-control" id="no_rfq" placeholder="No RFQ" name="no_rfq_receh" autofocus value="<?= $item['no_rfq']; ?>" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="no_wo" class="form-label">No WO</label>
                                            <input type="text" class="form-control" id="no_wo" placeholder="No WO" name="no_wo_receh" value="<?= $item['no_wo']; ?>" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="mb-3" hidden>
                                    <label for="name_cust" class="form-label">Name Customer</label>
                                    <input type="text" class="form-control" id="name_cust" placeholder="Name Customer" name="name_cust_receh" value="<?= $item['name_cust']; ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="code_qr" class="form-label">Code QR</label>
                                    <input type="text" class="form-control" id="code_qr" placeholder="Code_QR" name="code_qr_receh" value="<?= $item['code_qr']; ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="name_item" class="form-label">Name Item</label>
                                            <input type="text" class="form-control" id="name_item" placeholder="Name Item" name="name_item_receh" value="<?= $item['name_item']; ?>" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="mb-3" hidden>
                                    <label for="warehouse" class="form-label">Warehouse</label>
                                    <input type="text" class="form-control" id="warehouse" placeholder="Warehouse" name="warehouse_receh" value="<?= $item['warehouse']; ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="lot_del" class="form-label">Lot Delivery</label>
                                    <input type="text" class="form-control" id="lot_del" placeholder="Lot Devlivery_receh" name="lot_del" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div hidden class="mb-3">
                                    <label for="no_sdf" class="form-label">No SDF</label>
                                    <input type="text" class="form-control" id="no_sdf" placeholder="No SDF" name="no_sdf_receh" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="no_tag" class="form-label">No Tag</label>
                                    <input type="number" class="form-control" id="no_tag" placeholder="No_Tag" name="no_tag_receh" value="<?= $item['no_tag']; ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="desc_pn" class="form-label">Description PN</label>
                                    <input type="text" class="form-control" id="desc_pn" placeholder="Description PN" name="desc_pn_receh" value="<?= $item['desc_pn']; ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="bpid" class="form-label">BPID</label>
                                    <input type="text" class="form-control" id="bpid" placeholder="BPID" name="bpid_receh" value="<?= $item['bpid']; ?>" />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <!-- just show location detail -->
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="qty" class="form-label">QTY</label>
                                            <input type="number" class="form-control" id="qty" placeholder="QTY" name="qty_receh" value="<?= $item['qty']; ?>" />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="locationsDetail" class="form-label">Location</label>
                                            <input type="text" class="form-control" id="locationsDetail" placeholder="Locations Detail" value="<?= $item['locations']; ?>.<?= $item['sub_locations']; ?>" required readonly />
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <!-- end just show location datail -->
                                <div hidden class="mb-3" >
                                    <label for="id_data" class="form-label">ID</label>
                                    <input type="text" class="form-control" id="id_data" placeholder="id data" name="id_data" value="<?= $item['id']; ?>" required readonly />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="rack" class="form-label">Rack</label>
                                    <input type="text" class="form-control" id="rack" placeholder="Rack" name="rack_receh" value="<?= $item['rack']; ?>" required readonly />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="locations" class="form-label">Location</label>
                                    <input type="text" class="form-control" id="locations" placeholder="Locations" name="locations_receh" value="<?= $item['locations']; ?>" required readonly />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <div class="mb-3" hidden>
                                    <label for="sub_locations" class="form-label">Sub Location</label>
                                    <input type="text" class="form-control" id="sub_locations" placeholder="Sub Locations" name="sub_locations_receh" value="<?= $item['sub_locations']; ?>" required readonly />
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit">Submit form</button>
                            </form>

                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Scan QR-Code</h4>
                            <p class="sub-header">This form for edit item</p>
                            <!-- SCANNER -->
                            <!-- <video id="preview" src="" style="height: 250px;"></video> -->
                            <!-- SCANNER -->

                            <div class="mb-3">
                                <label for="code_qr" class="form-label">Code QR</label>
                                <input type="text" class="form-control" placeholder="input qr-code here" id="qr" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>

                            <script type="text/javascript">
                                // variabel yang tidak berubah
                                const input = document.getElementById('qr');
                                // const scanner = new Instascan.Scanner({
                                //     video: document.getElementById('preview')
                                // });

                                // fungsi untuk menangani respons dari permintaan GET
                                function handleResponse() {
                                    if (this.readyState === 4 && this.status === 200) {
                                        const data = JSON.parse(this.responseText);
                                        if (data.results[0].NO_RFQ === null || data.results[0].NO_RFQ === "") {
                                            alert("Data kosong");
                                        } else {
                                            alert("Data ada");
                                            document.getElementById('no_rfq_receh').value = data.results[0].NO_RFQ;
                                            document.getElementById('no_wo_receh').value = data.results[0].NO_WO;
                                            document.getElementById('name_cust_receh').value = data.results[0].CUSTOMER_NAME;
                                            document.getElementById('code_qr_receh').value = data.results[0].NOTE;
                                            document.getElementById('qty_receh').value = data.results[0].QTY;
                                            document.getElementById('warehouse_receh').value = data.results[0].WAREHOUSE;
                                            document.getElementById('no_tag_receh').value = data.results[0].NO_TAG;
                                            document.getElementById('name_item_receh').value = data.results[0].ITEM;
                                            document.getElementById('desc_pn_receh').value = data.results[0].DESCRIPTION_PN;
                                            document.getElementById('bpid_receh').value = data.results[0].BPID;
                                            document.getElementById('no_sdf_receh').value = data.results[0].SDF_ORDER;
                                            document.getElementById('lot_del_receh').value = data.results[0].QTY_LOT_DELIV;
                                        }
                                    } else if (this.readyState === 4) {
                                        alert("Tidak ada koneksi");
                                    }
                                }

                                // fungsi untuk mengirimkan permintaan GET
                                function sendGetRequest(url) {
                                    const xhr = new XMLHttpRequest();
                                    xhr.open('GET', url, true);
                                    xhr.onreadystatechange = handleResponse;
                                    xhr.send();
                                }

                                // event listener untuk input
                                input.addEventListener('input', function(event) {
                                    const content = event.target.value;
                                    const url = "https://portal2.incoe.astra.co.id/e-wip/api/getBarcodeId/" + content;
                                    sendGetRequest(url);
                                });

                                // event listener untuk scanner
                                // scanner.addListener('scan', function(content) {
                                //     const url = "https://portal2.incoe.astra.co.id/e-wip/api/getBarcodeId/" + content;
                                //     sendGetRequest(url);
                                // });

                                // memulai scanner
                                // Instascan.Camera.getCameras().then(function(cameras) {
                                //     if (cameras.length > 0) {
                                //         scanner.start(cameras[0]);
                                //     } else {
                                //         console.error('camera tidak di temukan');
                                //     }
                                // }).catch(function(e) {
                                //     console.error(e);
                                // });
                            </script>
                            <!-- end scan qr from api -->
                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
                </div> <!-- end col-->
                
            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


    <?= $this->endSection(); ?>