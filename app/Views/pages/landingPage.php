<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>

<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
            <h1 class="page-title text-center">Welcome To Warehouse</h1>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="responsive-table-plugin mt-3">
                                    <div class="table-rep-plugin">
                                        <h4 class="page-title">List Rack</h4>
                                        <div class="table-responsive" data-pattern="priority-columns">
                                            <table id="tech-companies-1" class="table table-striped">
                                                <thead>
                                                </thead>
                                                <tbody>
                                                    <td>
                                                        <div class="card bg-pattern">
                                                            <a href="/rackV">
                                                                <div class="card-body">
                                                                    <h4 class="text-center">Rack V</h4>
                                                                    <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:lightseagreen"></i>
                                                                </div>
                                                            </a>

                                                        </div> <!-- end card-->
                                                    </td>
                                                    <td>
                                                        <div class="card bg-pattern">
                                                            <a href="/rackW">
                                                                <div class="card-body">
                                                                <h4 class="text-center">Rack W</h4>
                                                                    <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:mediumpurple"></i>
                                                                </div>
                                                            </a>
                                                        </div> <!-- end card-->
                                                    </td>
                                                    <td>
                                                        <div class="card bg-pattern">
                                                            <a href="/rackX">
                                                                <div class="card-body">
                                                                <h4 class="text-center">Rack X</h4>
                                                                    <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:crimson"></i>
                                                                </div>
                                                            </a>
                                                        </div> <!-- end card-->
                                                    </td>
                                                    <td>
                                                        <div class="card bg-pattern">
                                                            <a href="/rackY">
                                                                <div class="card-body">
                                                                <h4 class="text-center">Rack Y</h4>
                                                                    <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:peru"></i>
                                                                </div>
                                                            </a>
                                                        </div> <!-- end card-->
                                                    </td>
                                                    <td>
                                                        <div class="card bg-pattern">
                                                            <a href="/rackOthers">
                                                                <div class="card-body">
                                                                <h4 class="text-center">Others</h4>
                                                                    <i class="fas fa-8x fa-warehouse d-flex justify-content-center" style="color:indigo"></i>
                                                                </div>
                                                            </a>
                                                        </div> <!-- end card-->
                                                    </td>

                                                </tbody>
                                            </table>
                                        </div> <!-- end .table-responsive -->

                                    </div> <!-- end .table-rep-plugin-->
                                </div> <!-- end .responsive-table-plugin-->

                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div>
            <!-- end row-->


        </div> <!-- container -->

    </div> <!-- content -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->


</div>
<!-- END wrapper -->

<?= $this->endSection(); ?>