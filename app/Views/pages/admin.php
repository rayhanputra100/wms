<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>
<?php
$uniqueRack = [];
$total = [];

foreach ($item as $v) {
    $rack = $v['rack'];
    //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya

    if (!in_array($rack, $uniqueRack)) {
        $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
    } else {
        continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
    }
    // $count = count($uniqueRack); //untuk menghitung total Rack
}
?>

<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title-box">
                            <h4 class="page-title">List Rack Items</h4>
                            <?php if (session()->getFlashdata('pesan')) : ?>
                                <div class="alert alert-success" role="alert">
                                    <?= session()->getFlashdata('pesan'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php foreach ($uniqueRack as $rack) : ?>
                                    <h4 class="text-center">Rack <?= $rack; ?></h4>
                                    <div class="responsive-table-plugin mt-3">
                                        <div class="table-rep-plugin">
                                            <div class="table-responsive" data-pattern="priority-columns">
                                                <table id="tech-companies-1" class="table table-striped">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                                        <?php if ($rack != "Others") : ?>
                                                            <?php for ($i = 5; $i >= 1; $i--) : ?>
                                                                <tr>
                                                                    <th>Rack <?= $rack ?><?= $i ?></th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $rack && $v['locations'] == $v['rack'] . $i) : ?>
                                                                            <?php if ($v['sub_locations'] == 500) : ?>
                                                                                <td></td>
                                                                            <?php else : ?>
                                                                                <td>
                                                                                    <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                        <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php else : ?>
                                                                                        <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                            <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                        </a>
                                                                                    <?php endif ?>
                                                                                </td>
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                </tr>
                                                            <?php endfor ?>
                                                        <?php else : ?>
                                                            <?php for ($i = 1; $i <= 7; $i++) : ?>
                                                                <tr>
                                                                    <th>Rack <?= $rack; ?> Area Transit <?= $i; ?></th>
                                                                    <?php foreach ($item as $v) : ?>
                                                                        <?php if ($v['rack'] === $rack && $v['locations'] == $v['rack'] . " Area Transit " . $i ) : ?>
                                                                            <td>
                                                                                <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                    <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                        <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                    </a>
                                                                                <?php else : ?>
                                                                                    <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                        <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                    </a>
                                                                                <?php endif ?>
                                                                            </td>
                                                                        <?php endif ?>
                                                                    <?php endforeach ?>
                                                                </tr>
                                                            <?php endfor ?>

                                                            <tr>
                                                                <th>Rack Gedung D</th>
                                                                <?php foreach ($item as $v) : ?>
                                                                    <?php if ($v['rack'] === $rack) : ?>
                                                                        <?php if ($v['rack'] === $rack && $v['locations'] == $v['rack'] . " Gedung D") : ?>
                                                                            <td>
                                                                                <?php if ($v['name_item_receh'] == NULL) : ?>
                                                                                    <a href="<?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? '/admin/' . $v['id'] : '/admin/detail/' . $v['id'] ?>" class="btn btn-xs d-flex <?= $v['code_qr'] === '' || $v['code_qr'] === NULL ? 'btn-danger' : 'btn-success' ?> " aria-expanded="false">
                                                                                        <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                    </a>
                                                                                <?php else : ?>
                                                                                    <a href="/admin/detail/<?= $v['id']; ?>" class="btn btn-xs d-flex btn-success " aria-expanded="false">
                                                                                        <?= $v['locations']; ?>.<?= $v['sub_locations']; ?><i class="mdi mdi-chevron-right"></i>
                                                                                    </a>
                                                                                <?php endif ?>
                                                                            </td>
                                                                        <?php endif ?>
                                                                    <?php endif ?>
                                                                <?php endforeach ?>
                                                            </tr>
                                                        <?php endif ?>

                                                    </tbody>
                                                </table>
                                            </div> <!-- end .table-responsive -->

                                        </div> <!-- end .table-rep-plugin-->
                                    </div> <!-- end .responsive-table-plugin-->
                                <?php endforeach ?>
                            </div>
                        </div> <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div>
            <!-- end row-->


        </div> <!-- container -->

    </div> <!-- content -->

    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->


</div>
<!-- END wrapper -->


<?= $this->endSection(); ?>