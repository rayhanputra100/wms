<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>
<?php
$uniqueRack = []; //array penampung nama supplier yang sudah unik
//melakukan iterasi terhadap hasil fetch data dari supplier
foreach ($item as $v) {
    $rack = $v['rack'];
    //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
    if (!in_array($rack, $uniqueRack)) {
        $uniqueRack[] = $rack; //tambahkan ke array uniqueSupplierNames
    } else {
        continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
    }
    // $count = count($uniqueRack); //untuk menghitung total Rack
}


?>

<?php

use App\Models\TItems;

$itemsModel = new TItems();
$itemsResult = $itemsModel->findAll();

if (!empty($_POST)) {
    foreach ($_POST['radioReset'] as $idItems) {
        $query = $itemsModel->update($idItems, [
            'no_rfq' => "",
            'no_sdf' => "",
            'lot_del' => "",
            'no_wo' => "",
            'name_cust' => "",
            'code_qr' => "",
            'qty' => "",
            'warehouse' => "",
            'no_tag' => "",
            'name_item' => "",
            'desc_pn' => "",
            'bpid' => ""
        ]);
    }
    return redirect()->to('/listdata');
    exit;
}
?>




<!-- Begin page -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- Warning Alert Modal -->
                                <div id="warning-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-body p-4">
                                                <div class="text-center">
                                                    <i class="dripicons-warning h1 text-warning"></i>
                                                    <h4 class="mt-2">Warning</h4>
                                                    <p class="mt-3">Button akan menghapus data tanpa melakukan scan QR code terlebih dahulu. Apa anda yakin ?</p>
                                                    <button type="button" class="btn btn-sm btn-light my-2" data-bs-dismiss="modal">Tidak</button>
                                                    <button id="demo-delete-row-paksa" class="btn btn-sm btn-danger ms-1" disabled><i class="mdi mdi-close me-1"></i>Ya</button>
                                                </div>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                                <div class="float-sm-end mb-2 mb-sm-0">
                                    <div class="row g-2">
                                        <div class="col-auto">
                                            <a href="/listdatareceh" class="btn btn-md btn-link">List Data Receh <i class="mdi mdi-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div> <!-- end dropdown-->
                                <h4 class="header-title">List Data Warehouse</h4>
                                <!-- <button class="btn btn-success btn-sm export-btn my-1"><i class="fas fa-file-excel"></i> Download</button> -->
                                <a href="/export" class="btn btn-success btn-sm my-1"><i class="fas fa-file-excel"></i> Download</a>
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <div class="btn-group mb-2 dropend">
                                        <button type="button" class="btn btn-sm btn-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pilih Reset<i class="mdi mdi-chevron-right"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <button id="demo-delete-row-biasa" class="btn btn-sm btn-danger me-1 mb-1" disabled><i class="mdi mdi-close me-1"></i>Reset</button>
                                            <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#warning-alert-modal"><i class="mdi mdi-close me-1"></i>Reset Paksa !</button>

                                        </div>
                                    </div>

                                    <table id="demo-custom-toolbar" data-toggle="table" data-toolbar="#demo-delete-row" data-search="true" data-sort-name="id" data-page-list="[5, 20, 50]" data-page-size="50" data-pagination="true" data-show-pagination-switch="true" class="table-borderless">
                                        <thead class="table-light">
                                            <tr class="text-center">
                                                <th data-field="state" data-checkbox="true"></th>
                                                <th>NO</th>
                                                <th>ID</th>
                                                <th>CUSTOMER</th>
                                                <th>ITEM</th>
                                                <th>NO RFQ</th>
                                                <th>NO WO</th>
                                                <th>NO SDF</th>
                                                <th>QTY</th>
                                                <th>LOT DELIVERY</th>
                                                <th>LOCATION</th>

                                            </tr>

                                        </thead>


                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($item as $v) : ?>
                                                <?php if (!empty($v['no_rfq'] && $v['no_wo'])) : ?>
                                                    <tr class="text-center">
                                                        <td></td>
                                                        <td><?= $i++; ?></td>
                                                        <td><?= $v['id']; ?></td>
                                                        <?php if ($v['name_cust'] == "") : ?>
                                                            <td>AOP Domestik</td>
                                                        <?php else : ?>
                                                            <td><?= $v['name_cust']; ?></td>
                                                        <?php endif ?>
                                                        <td><?= $v['name_item']; ?></td>
                                                        <td><?= $v['no_rfq']; ?></td>
                                                        <td><?= $v['no_wo']; ?></td>
                                                        <td><?= $v['no_sdf']; ?></td>
                                                        <td><?= $v['qty']; ?></td>
                                                        <td><?= $v['lot_del']; ?></td>
                                                        <td><?= $v['locations']; ?>.<?= $v['sub_locations']; ?></td>

                                                        <!-- <td><img src="../assets/images/qrcode/listdata/item-<?= $v["id"]; ?>.png" alt="qritem-<?= $v["id"]; ?>" class="img-fluid w-25"></td> -->

                                                        <!-- <a class="btn btn-xs btn-success me-1" href="/gqr/<?= $v['id']; ?>">Generate QR</a>
                                                        <a class="btn btn-xs btn-success me-1" href="/admin/detail/<?= $v['id']; ?>">Detail</a>
                                                        <a class="btn btn-xs btn-warning me-1" href="/listdata/<?= $v['id']; ?>">Edit</a> -->
                                                        <!-- <a class="btn btn-xs btn-warning me-1" href="/listreset/<?= $v['id']; ?>">Reset</a> -->


                                                    </tr>


                                                <?php endif ?>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>

            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Warehouse Management System by <a href="">RayhanPJ</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->
<?= $this->endSection(); ?>