<!-- Topbar Start -->
<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-end mb-0">

            <li class="dropdown d-none d-lg-inline-block">
                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="fullscreen" href="#">
                    <i class="fe-maximize noti-icon"></i>
                </a>
            </li>
            
            <!-- <li class="dropdown notification-list">
                <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">
                    <i class="fe-settings noti-icon"></i>
                </a>
            </li> -->

        </ul>

        <!-- LOGO -->
        <div class="logo-box">
            <a href="index.html" class="logo logo-dark text-center">
                <span class="logo-sm">
                    <img src="../assets/images/iPTCBI.png" alt="" height="22">
                    <!-- <span class="logo-lg-text-light">UBold</span> -->
                </span>
                <span class="logo-lg">
                    <img src="../assets/images/iPTCBI.png" alt="" height="20">
                    <!-- <span class="logo-lg-text-light">U</span> -->
                </span>
            </a>

            <a href="index.html" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img src="../assets/images/iPTCBI.png" alt="" height="22">
                </span>
                <span class="logo-lg">
                    <img src="../assets/images/iPTCBI.png" alt="" height="60">
                </span>
            </a>
        </div>

        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                    <i class="fe-menu"></i>
                </button>
            </li>

            <li>
                <!-- Mobile menu toggle (Horizontal Layout)-->
                <a class="navbar-toggle nav-link" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>

        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!-- end Topbar -->

<div class="topnav">
    <div class="container-fluid">
        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

            <div class="collapse navbar-collapse" id="topnav-menu-content">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-dashboard" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fe-airplay me-1"></i> Dashboards <div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="topnav-dashboard">
                            <a href="<?= base_url(); ?>" class="dropdown-item">Home</a>
                            <!-- <a href="<?= base_url(); ?>homeV2" class="dropdown-item">Home (lite)</a> -->
                            <a href="<?= base_url(); ?>admin" class="dropdown-item">All Rack</a>
                            <a href="<?= base_url(); ?>admin/scan" class="dropdown-item">Form Input Data From Scan</a>
                            <a href="<?= base_url(); ?>admin/create" class="dropdown-item">Form Input Rack & Location</a>
                            <!-- <a href="<?= base_url(); ?>adminqr" class="dropdown-item">Form Input & Generate QR-Code</a> -->
                            <a href="<?= base_url(); ?>listdata" class="dropdown-item">List Data</a>
                            <!-- <a href="<?= base_url(); ?>checkItems" class="dropdown-item">Check Data</a> -->
                        </div>
                    </li>

                </ul> <!-- end navbar-->
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-dashboard" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-warehouse"></i> Rack WH <div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="topnav-dashboard">
                            <a href="<?= base_url(); ?>rackV" class="dropdown-item">Rack V</a>
                            <a href="<?= base_url(); ?>rackW" class="dropdown-item">Rack W</a>
                            <a href="<?= base_url(); ?>rackX" class="dropdown-item">Rack X</a>
                            <a href="<?= base_url(); ?>rackY" class="dropdown-item">Rack Y</a>
                            <a href="<?= base_url(); ?>rackOthers" class="dropdown-item">Rack Others</a>
                        </div>
                    </li>

                </ul> <!-- end navbar-->
            </div> <!-- end .collapsed-->
        </nav>
    </div> <!-- end container-fluid -->
</div> <!-- end topnav-->