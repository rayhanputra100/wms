<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Dashboard Warehouse</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Warehouse Management System" name="Rayhan" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Bootstrap Tables css -->
    <link href="<?php echo base_url(); ?>assets/libs/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />

    <!-- My CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mycss.css">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/iPTCBI.ico">

    <!-- Plugins css -->
    <link href="<?php echo base_url(); ?>assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="<?php echo base_url(); ?>assets/css/config/creative/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/config/creative/app.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

    <link href="<?php echo base_url(); ?>assets/css/config/creative/bootstrap-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/config/creative/app-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />

    <!-- third party css -->
    <link href="<?php echo base_url(); ?>assets/libs/datatables.net-bs5/css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/libs/datatables.net-responsive-bs5/css/responsive.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/libs/datatables.net-buttons-bs5/css/buttons.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/libs/datatables.net-select-bs5/css//select.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
    <!-- icons -->
    <link href="<?php echo base_url(); ?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
</head>

<!-- body start -->

<body class="loading" data-layout-mode="horizontal" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": false}, "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": true}'>

    <?= $this->include('/layouts/navbar'); ?>
    <br />
    <?= $this->renderSection('content'); ?>

    <!-- Right Sidebar -->
    <!-- <div class="right-bar">
            <div data-simplebar class="h-100"> -->

    <!-- Nav tabs -->
    <!-- <ul class="nav nav-tabs nav-bordered nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link py-2 active" data-bs-toggle="tab" href="#settings-tab" role="tab">
                            <i class="mdi mdi-cog-outline d-block font-22 my-1"></i>
                        </a>
                    </li>
                </ul> -->

    <!-- Tab panes -->
    <!-- <div class="tab-content pt-0">  
                    <div class="tab-pane active" id="settings-tab" role="tabpanel">
                        <h6 class="fw-medium px-3 m-0 py-2 font-13 text-uppercase bg-light">
                            <span class="d-block py-1">Theme Settings</span>
                        </h6>

                        <div class="p-3">
                            <div class="alert alert-warning" role="alert">
                                <strong>Customize </strong> the overall color scheme, sidebar menu, etc.
                            </div>

                            <h6 class="fw-medium font-14 mt-4 mb-2 pb-1">Color Scheme</h6>
                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="color-scheme-mode" value="light" id="light-mode-check" checked />
                                <label class="form-check-label" for="light-mode-check">Light Mode</label>
                            </div>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="color-scheme-mode" value="dark" id="dark-mode-check" />
                                <label class="form-check-label" for="dark-mode-check">Dark Mode</label>
                            </div> -->

    <!-- Width -->
    <!-- <h6 class="fw-medium font-14 mt-4 mb-2 pb-1">Width</h6>
                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="width" value="fluid" id="fluid-check" checked />
                                <label class="form-check-label" for="fluid-check">Fluid</label>
                            </div>
                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="width" value="boxed" id="boxed-check" />
                                <label class="form-check-label" for="boxed-check">Boxed</label>
                            </div> -->

    <!-- Menu positions -->
    <!-- <h6 class="fw-medium font-14 mt-4 mb-2 pb-1">Layout Positon</h6>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="menus-position" value="fixed" id="fixed-check" checked />
                                <label class="form-check-label" for="fixed-check">Fixed</label>
                            </div>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="menus-position" value="scrollable" id="scrollable-check" />
                                <label class="form-check-label" for="scrollable-check">Scrollable</label>
                            </div> -->

    <!-- Topbar -->
    <!-- <h6 class="fw-medium font-14 mt-4 mb-2 pb-1">Topbar</h6>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="topbar-color" value="dark" id="darktopbar-check" checked />
                                <label class="form-check-label" for="darktopbar-check">Dark</label>
                            </div>

                            <div class="form-check form-switch mb-1">
                                <input type="checkbox" class="form-check-input" name="topbar-color" value="light" id="lighttopbar-check" />
                                <label class="form-check-label" for="lighttopbar-check">Light</label>
                            </div>


                            <div class="d-grid mt-4">
                                <button class="btn btn-primary" id="resetBtn">Reset to Default</button>
                            </div>

                        </div>

                    </div>
                </div>

            </div> end slimscroll-menu -->
    <!-- </div> -->
    <!-- /Right-bar -->

    <!-- Right bar overlay-->
    <!-- <div class="rightbar-overlay"></div> -->
    <!-- Vendor js -->
    <script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>

    <!-- Plugins js-->
    <script src="<?php echo base_url(); ?>assets/libs/flatpickr/flatpickr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/apexcharts/apexcharts.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/libs/selectize/js/standalone/selectize.min.js"></script>

    <!-- third party js -->
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/datatables.net-select/js/dataTables.select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tableHTMLExport.js"></script>
    <!-- third party js ends -->


    <!-- Instascan -->
    <!-- <script src="<?php echo base_url(); ?>assets/js/adapter.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/instascan.min.js"></script> -->

    <!-- Datatables init -->
    <script src="<?php echo base_url(); ?>assets/js/pages/datatables.init.js"></script>

    <!-- Bootstrap Tables js -->
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap-table/bootstrap-table.min.js"></script>
    <!-- Init js -->
    <script src="<?php echo base_url(); ?>assets/js/pages/bootstrap-tables.init.js"></script>
    <!-- Dashboar 1 init js-->
    <script src="<?php echo base_url(); ?>assets/js/pages/dashboard-1.init.js"></script>

    <!-- App js-->
    <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
    
    <!-- Reset data with scan qr -->
    <script>
        $(document).ready(function() {
            var t = $("#demo-custom-toolbar"),
                n = $("#demo-delete-row-biasa");

            t.on(
                "check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table",
                function() {
                    n.prop("disabled", !t.bootstrapTable("getSelections").length);
                }
            );

            n.click(function() {
                var e = $.map(t.bootstrapTable("getSelections"), function(e) {
                    return e[2];
                });

                if (e.length > 0) {
                    var codeQRInput = prompt("Please enter the code QR values separated by spaces:");

                    if (codeQRInput !== null && codeQRInput.trim() !== "") {
                        var noRfqArray = codeQRInput.trim().split(" ");
                        var foundIds = [];
                        var ids = "";

                        <?php foreach ($item as $v) { ?>
                            <?php if (isset($v['code_qr_receh']) && is_string($v['code_qr_receh'])) { ?>
                                var codeQR = "<?= $v['code_qr_receh']; ?>";
                                if (noRfqArray.includes(codeQR)) {
                                    foundIds.push("<?= $v['id_receh']; ?>");
                                    ids += "<?= $v['id_receh']; ?>,";
                                }
                            <?php } ?>
                        <?php } ?>

                        if (foundIds.length > 0) {
                            ids = ids.replace(/,$/, "");
                            var idArray = ids.split(",");
                            var filteredIds = e.filter(function(id) {
                                return idArray.includes(id);
                            });

                            if (filteredIds.length === e.length) {
                                $.each(e, function(index, id) {
                                    $.ajax({
                                        url: '/admin/delreceh/' + id,
                                        type: 'GET',
                                        success: function(response) {
                                            console.log('Reset data berhasil untuk ID ' + id);
                                        },
                                        error: function(xhr, status, error) {
                                            console.log('Reset data gagal untuk ID ' + id);
                                        }
                                    });
                                });

                                // Arahkan pengguna ke halaman `/listdatareceh`
                                window.location.href = '/listdatareceh';
                            } else {
                                alert("IDs do not have the same no_rfq values. Please enter the same no_rfq values for resetting.");
                            }
                        } else {
                            alert("No matching no_rfq found. Please enter valid no_rfq values for resetting.");
                        }
                    } else {
                        alert("no_rfq input is empty or canceled. Please enter the same no_rfq values for resetting.");
                    }
                } else {
                    alert("Please select at least one row.");
                }
            });
        });
    </script>
    <!-- Reset data Force! -->
    <script>
        $(document).ready(function() {
            var t = $("#demo-custom-toolbar"),
                n = $("#demo-delete-row-paksa");
            t.on(
                    "check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table",
                    function() {
                        n.prop("disabled", !t.bootstrapTable("getSelections").length);
                    }
                ),
                n.click(function() {
                    var e = $.map(t.bootstrapTable("getSelections"), function(e) {
                        return e[2];
                    });
                    if (e.length > 0) {
                        // Mengubah nilai dari $id_array dengan nilai array e
                        var id_array = e;
                        // Lakukan iterasi pada setiap ID dan memanggil metode update untuk masing-masing ID
                        $.each(id_array, function(index, id) {
                            $.ajax({
                                url: '/listreset/' + id,
                                type: 'GET',
                                success: function(response) {
                                    // Lakukan sesuatu jika metode update berhasil
                                    console.log('Reset data berhasil untuk ID ' + id);
                                },
                                error: function(xhr, status, error) {
                                    // Lakukan sesuatu jika metode update gagal
                                    console.log('Reset data gagal untuk ID ' + id);
                                }
                            });
                        });
                        // Arahkan pengguna ke halaman `/listdata`
                        window.location.href = '/listdata';
                    } else {
                        // Jika tidak ada baris yang dipilih, maka tampilkan alert kepada pengguna.
                        alert("Please select at least one row.");
                    }
                });
        })
    </script>
    <!-- Reset data receh with scan qr  -->
    <script>
        $(document).ready(function() {
            var t = $("#custom-toolbar-receh"),
                n = $("#delete-row-receh-biasa");

            t.on(
                "check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table",
                function() {
                    n.prop("disabled", !t.bootstrapTable("getSelections").length);
                }
            );

            n.click(function() {
                var e = $.map(t.bootstrapTable("getSelections"), function(e) {
                    return e[2];
                });

                if (e.length > 0) {
                    var codeQRInput = prompt("Please enter the code QR values separated by spaces:");

                    if (codeQRInput !== null && codeQRInput.trim() !== "") {
                        var noRfqArray = codeQRInput.trim().split(" ");
                        var foundIds = [];
                        var ids = "";

                        <?php foreach ($item as $v) { ?>
                            <?php if (isset($v['code_qr_receh']) && is_string($v['code_qr_receh'])) { ?>
                                var codeQR = "<?= $v['code_qr_receh']; ?>";
                                if (noRfqArray.includes(codeQR)) {
                                    foundIds.push("<?= $v['id_receh']; ?>");
                                    ids += "<?= $v['id_receh']; ?>,";
                                }
                            <?php } ?>
                        <?php } ?>

                        if (foundIds.length > 0) {
                            ids = ids.replace(/,$/, "");
                            var idArray = ids.split(",");
                            var filteredIds = e.filter(function(id) {
                                return idArray.includes(id);
                            });

                            if (filteredIds.length === e.length) {
                                $.each(e, function(index, id) {
                                    $.ajax({
                                        url: '/admin/delreceh/' + id,
                                        type: 'GET',
                                        success: function(response) {
                                            console.log('Reset data berhasil untuk ID ' + id);
                                        },
                                        error: function(xhr, status, error) {
                                            console.log('Reset data gagal untuk ID ' + id);
                                        }
                                    });
                                });

                                // Arahkan pengguna ke halaman `/listdatareceh`
                                window.location.href = '/listdatareceh';
                            } else {
                                alert("IDs do not have the same no_rfq values. Please enter the same no_rfq values for resetting.");
                            }
                        } else {
                            alert("No matching no_rfq found. Please enter valid no_rfq values for resetting.");
                        }
                    } else {
                        alert("no_rfq input is empty or canceled. Please enter the same no_rfq values for resetting.");
                    }
                } else {
                    alert("Please select at least one row.");
                }
            });
        });
    </script>
    <!-- Reset data receh Force! -->
    <script>
        $(document).ready(function() {
            var t = $("#custom-toolbar-receh"),
                n = $("#delete-row-receh-paksa");
            t.on(
                    "check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table",
                    function() {
                        n.prop("disabled", !t.bootstrapTable("getSelections").length);
                    }
                ),
                n.click(function() {
                    var e = $.map(t.bootstrapTable("getSelections"), function(e) {
                        return e[2];
                    });
                    if (e.length > 0) {
                        // Mengubah nilai dari $id_array dengan nilai array e
                        var id_array = e;
                        // Lakukan iterasi pada setiap ID dan memanggil metode update untuk masing-masing ID
                        $.each(id_array, function(index, id) {
                            $.ajax({
                                url: '/listreset/' + id,
                                type: 'GET',
                                success: function(response) {
                                    // Lakukan sesuatu jika metode update berhasil
                                    console.log('Reset data berhasil untuk ID ' + id);
                                },
                                error: function(xhr, status, error) {
                                    // Lakukan sesuatu jika metode update gagal
                                    console.log('Reset data gagal untuk ID ' + id);
                                }
                            });
                        });
                        // Arahkan pengguna ke halaman `/listdata`
                        window.location.href = '/listdata';
                    } else {
                        // Jika tidak ada baris yang dipilih, maka tampilkan alert kepada pengguna.
                        alert("Please select at least one row.");
                    }
                });
        })
    </script>




</body>

</html>