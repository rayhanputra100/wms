<?php

namespace App\Controllers;

use App\Models\TItems;
use App\Models\TBackup;
use App\Models\TReceh;
use App\Models\TOut;

class Form extends BaseController
{
    protected $tItems;
    protected $tBackup;
    protected $tReceh;
    protected $tOut;
    public function __construct()
    {
        $this->tItems = new TItems();
        $this->tBackup = new TBackup();
        $this->tReceh = new TReceh();
        $this->tOut = new TOut();
    }

    public function index()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/form/formInput', $data);
    }

    public function check()
    {
        $data = [
            'title' => 'Check Item',
            'validation' => \Config\Services::validation(),
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/form/formCheckItems', $data);
    }

    public function bypass($id)
    {
        $idBypass = $this->request->getVar('id');
        $this->tItems->save([
            'id' => $this->request->getVar('id'),
            'no_rfq' => ($this->request->getVar('no_rfq') == "") ? "Manual" : $this->request->getVar('no_rfq'),
            'no_wo' => ($this->request->getVar('no_wo') == "") ? "Manual" : $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust'),
            'code_qr' => ($this->request->getVar('code_qr') == "") ? "Manual" : $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty'),
            'locations' => $this->request->getVar('locations'),
            'sub_locations' => $this->request->getVar('sub_locations'),
            'rack' => $this->request->getVar('rack'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
            'no_sdf' => $this->request->getVar('no_sdf'),
            'lot_del' => $this->request->getVar('lot_del'),
        ]);

        $this->tItems->update($id, array(
            'no_rfq' => NULL,
            'no_sdf' => NULL,
            'lot_del' => NULL,
            'no_wo' => NULL,
            'name_cust' => NULL,
            'code_qr' => NULL,
            'qty' => 0,
            'warehouse' => NULL,
            'no_tag' => NULL,
            'name_item' => NULL,
            'desc_pn' => NULL,
            'bpid' => NULL
        ));

        return redirect()->to('/admin/detail/' . $idBypass);
    }

    public function fetch()
    {
        $areaModel = model(TItems::class);
        $areaResult = $areaModel->findAll();

        return $this->response->setJSON($areaResult);
    }

    public function update($id)
    {
        $rack = $this->request->getVar('rack');
        $this->tItems->save([
            'id' => $id,
            'no_rfq' => ($this->request->getVar('no_rfq') == "") ? "Manual" : $this->request->getVar('no_rfq'),
            'no_wo' => ($this->request->getVar('no_wo') == "") ? "Manual" : $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust'),
            'code_qr' => ($this->request->getVar('code_qr') == "") ? "Manual" : $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty'),
            'locations' => $this->request->getVar('locations'),
            'sub_locations' => $this->request->getVar('sub_locations'),
            'rack' => $this->request->getVar('rack'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
            'no_sdf' => $this->request->getVar('no_sdf'),
            'lot_del' => $this->request->getVar('lot_del'),
        ]);
        $this->tBackup->insert([
            'id' => $id,
            'no_rfq' => $this->request->getVar('no_rfq') ? "Manual" : $this->request->getVar('no_rfq'),
            'no_wo' => $this->request->getVar('no_wo') ? "Manual" : $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust') ? "Anony" : $this->request->getVar('name_cust'),
            'code_qr' => $this->request->getVar('code_qr') ? "Manual" : $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty') === "" ? 0 : $this->request->getVar('qty'),
            'locations' => $this->request->getVar('locations'),
            'sub_locations' => $this->request->getVar('sub_locations'),
            'rack' => $this->request->getVar('rack'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
            'no_sdf' => $this->request->getVar('no_sdf'),
            'lot_del' => $this->request->getVar('lot_del'),
        ]);

        session()->setFlashdata('pesan', 'Data Berhasil Diubah');
        return redirect()->to('/rack' . $rack);
    }

    public function scanning()
    {
        // dd($this->request->getVar('id'));
        $location = $this->request->getVar('locations');
        $subLocation = $this->request->getVar('sub_locations');
        $this->tItems->save([
            'id' => $this->request->getVar('id'),
            'no_rfq' => ($this->request->getVar('no_rfq') == "") ? "Manual" : $this->request->getVar('no_rfq'),
            'no_wo' => ($this->request->getVar('no_wo') == "") ? "Manual" : $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust') ? "Anony" : $this->request->getVar('name_cust'),
            'code_qr' => ($this->request->getVar('code_qr') == "") ? "Manual" : $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty') == "" ? 0 : $this->request->getVar('qty'),
            'locations' => $this->request->getVar('locations'),
            'sub_locations' => $this->request->getVar('sub_locations'),
            'rack' => $this->request->getVar('rack'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
            'no_sdf' => $this->request->getVar('no_sdf'),
            'lot_del' => $this->request->getVar('lot_del'),
        ]);
        $this->tBackup->insert([
            'id' => $this->request->getVar('id'),
            'no_rfq' => ($this->request->getVar('no_rfq') == "") ? "Manual" : $this->request->getVar('no_rfq'),
            'no_wo' => ($this->request->getVar('no_wo') == "") ? "Manual" : $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust') ? "Anony" : $this->request->getVar('name_cust'),
            'code_qr' => ($this->request->getVar('code_qr') == "") ? "Manual" : $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty') == "" ? 0 : $this->request->getVar('qty'),
            'locations' => $this->request->getVar('locations'),
            'sub_locations' => $this->request->getVar('sub_locations'),
            'rack' => $this->request->getVar('rack'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
            'no_sdf' => $this->request->getVar('no_sdf'),
            'lot_del' => $this->request->getVar('lot_del'),
        ]);

        session()->setFlashdata('pesan', "Data Berhasil Diubah di Rack {$location}.{$subLocation}");
        return redirect()->to('/admin/scan');
    }

    public function updateData($id)
    {
        $this->tItems->save([
            'id' => $id,
            'no_rfq' => ($this->request->getVar('no_rfq') == "") ? "Manual" : $this->request->getVar('no_rfq'),
            'no_wo' => ($this->request->getVar('no_wo') == "") ? "Manual" : $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust') ? "Anony" : $this->request->getVar('name_cust'),
            'code_qr' => ($this->request->getVar('code_qr') == "") ? "Manual" : $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty') == "" ? 0 : $this->request->getVar('qty'),
            'locations' => $this->request->getVar('locations'),
            'sub_locations' => $this->request->getVar('sub_locations'),
            'rack' => $this->request->getVar('rack'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
            'no_sdf' => $this->request->getVar('no_sdf'),
            'lot_del' => $this->request->getVar('lot_del'),
        ]);
        $this->tBackup->save([
            'id' => $id,
            'no_rfq' => ($this->request->getVar('no_rfq') == "") ? "Manual" : $this->request->getVar('no_rfq'),
            'no_wo' => ($this->request->getVar('no_wo') == "") ? "Manual" : $this->request->getVar('no_wo'),
            'name_cust' => $this->request->getVar('name_cust') ? "Anony" : $this->request->getVar('name_cust'),
            'code_qr' => ($this->request->getVar('code_qr') == "") ? "Manual" : $this->request->getVar('code_qr'),
            'qty' => $this->request->getVar('qty') == "" ? 0 : $this->request->getVar('qty'),
            'locations' => $this->request->getVar('locations'),
            'sub_locations' => $this->request->getVar('sub_locations'),
            'rack' => $this->request->getVar('rack'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
            'no_sdf' => $this->request->getVar('no_sdf'),
            'lot_del' => $this->request->getVar('lot_del'),
        ]);

        session()->setFlashdata('pesan', 'Data Berhasil Diubah');
        return redirect()->to('/listdata');
    }

    public function create()
    {
        session();
        $data = [
            'title' => 'Form Input Item',
            'item' => $this->tItems->getItem(),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/form/formInput', $data);
    }

    public function tambahForm($id)
    {
        session();
        $data = [
            'title' => 'Form Input Item',
            'item' => $this->tItems->getItem($id),
            'validation' => \Config\Services::validation()
        ];
        $data2 = [
            'item' => $this->tReceh->getItem($id),
            'validation' => \Config\Services::validation()
        ];

        return view('pages/form/formTambahDataReceh', $data, $data2);
    }

    public function save()
    {
        if (!$this->validate([
            'sub_locations' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Sub Location harus di isi!',
                ]
            ]
        ])) {
            $validation = \Config\Services::validation();
            // return redirect()->to('/admin/create')->withInput()->with('validation', $validation);
            $data = [
                'title' => 'Form Input Item',
                'item' => $this->tItems->getItem(),
                'validation' => $validation
            ];
            return view('pages/form/formInput', $data);
        }
        $rack = $this->request->getVar('rack');
        $this->tItems->save([
            'no_rfq' => $this->request->getVar('no_rfq') ,
            'no_wo' => $this->request->getVar('no_wo') ,
            'name_cust' => $this->request->getVar('name_cust'),
            'code_qr' => $this->request->getVar('code_qr') ,
            'qty' => $this->request->getVar('qty') ,
            'locations' => $this->request->getVar('locations'),
            'sub_locations' => $this->request->getVar('sub_locations'),
            'rack' => $this->request->getVar('rack'),
            'warehouse' => $this->request->getVar('warehouse'),
            'no_tag' => $this->request->getVar('no_tag'),
            'desc_pn' => $this->request->getVar('desc_pn'),
            'name_item' => $this->request->getVar('name_item'),
            'bpid' => $this->request->getVar('bpid'),
            'no_sdf' => $this->request->getVar('no_sdf'),
            'lot_del' => $this->request->getVar('lot_del'),
        ]);

        session()->setFlashdata('pesan', 'Data Berhasil Ditambahkan');
        return redirect()->to('/rack' . $rack);
    }

    public function tambahDataReceh()
    {
        $idData = $this->request->getVar('id_data');
        $rack = $this->request->getVar('rack_receh');
        $this->tReceh->save([
            'id_data' => $this->request->getVar('id_data'),
            'no_rfq_receh' => $this->request->getVar('no_rfq_receh'),
            'no_wo_receh' => $this->request->getVar('no_wo_receh'),
            'name_cust_receh' => $this->request->getVar('name_cust_receh'),
            'code_qr_receh' => $this->request->getVar('code_qr_receh'),
            'qty_receh' => $this->request->getVar('qty_receh'),
            'locations_receh' => $this->request->getVar('locations_receh'),
            'sub_locations_receh' => $this->request->getVar('sub_locations_receh'),
            'rack_receh' => $this->request->getVar('rack_receh'),
            'warehouse_receh' => $this->request->getVar('warehouse_receh'),
            'no_tag_receh' => $this->request->getVar('no_tag_receh'),
            'desc_pn_receh' => $this->request->getVar('desc_pn'),
            'name_item_receh' => $this->request->getVar('name_item_receh'),
            'bpid_receh' => $this->request->getVar('bpid_receh'),
            'no_sdf_receh' => $this->request->getVar('no_sdf_receh'),
            'lot_del_receh' => $this->request->getVar('lot_del_receh'),
        ]);

        $this->tItems->update($idData, [
            'name_item_receh' => $this->request->getVar('name_item_receh'),
        ]);

        return redirect()->to('/rack' . $rack);
    }

    public function updateDataReceh($id_receh)
    {
        $idData = $this->request->getVar('id_data');
        $this->tReceh->save([
            'id_receh' => $id_receh,
            'id_data' => $this->request->getVar('id_data'),
            'no_rfq_receh' => $this->request->getVar('no_rfq_receh'),
            'no_wo_receh' => $this->request->getVar('no_wo_receh'),
            'name_cust_receh' => $this->request->getVar('name_cust_receh'),
            'code_qr_receh' => $this->request->getVar('code_qr_receh'),
            'qty_receh' => $this->request->getVar('qty_receh'),
            'locations_receh' => $this->request->getVar('locations_receh'),
            'sub_locations_receh' => $this->request->getVar('sub_locations_receh'),
            'rack_receh' => $this->request->getVar('rack_receh'),
            'warehouse_receh' => $this->request->getVar('warehouse_receh'),
            'no_tag_receh' => $this->request->getVar('no_tag_receh'),
            'desc_pn_receh' => $this->request->getVar('desc_pn'),
            'name_item_receh' => $this->request->getVar('name_item_receh'),
            'bpid_receh' => $this->request->getVar('bpid_receh'),
            'no_sdf_receh' => $this->request->getVar('no_sdf_receh'),
            'lot_del_receh' => $this->request->getVar('lot_del_receh'),
        ]);

        return redirect()->to('/admin/detail/' . $idData);
    }

    public function reset($id)
    {
        // Mengupdate data pada ID yang diberikan
        $this->tItems->update($id, array(
            'no_rfq' => NULL,
            'no_sdf' => NULL,
            'lot_del' => NULL,
            'no_wo' => NULL,
            'name_cust' => NULL,
            'code_qr' => NULL,
            'qty' => 0,
            'warehouse' => NULL,
            'no_tag' => NULL,
            'name_item' => NULL,
            'desc_pn' => NULL,
            'bpid' => NULL
        ));

        return;
    }
}
