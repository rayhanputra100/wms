<?php

namespace App\Controllers;

use App\Models\TBackup;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class ListDataTemp extends BaseController
{
    
    protected $tBackup;
    public function __construct()
    {
        $this->tBackup = new TBackup();
    }

    public function index()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tBackup->getItem(),
        ];
        return view('pages/listDataTemp', $data);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tBackup->getItem($id),
        ];
        return view('pages/detail', $data);
    }

    public function json($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tBackup->getItem($id),
        ];
        return $this->response->setJSON($data);
    }

    public function qr($id)
    {

        $writer = new PngWriter();

        // Create QR code
        $qrCode = QrCode::create('http://localhost:8080/json/'.$id)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);
        $result->saveToFile('assets/images/qrcode/listdata/item-'.$id.'.png');
        return redirect()->to('/listdata');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Form Ubah Data Item',
            'validation' => \Config\Services::validation(),
            'item' => $this->tBackup->getItem($id)

        ];

        return view('pages/form/formEditData', $data);
    }

    public function delete($id)
    {
        $this->tBackup->delete($id);

        return redirect()->to('/admin');
    }

    public function reset($id)
    {
        $this->tBackup->update($id, array('no_rfq' => "", 'no_wo' => "", 'name_cust' => "", 'code_qr' => "", 'qty' => "",'warehouse' => "",'no_tag' => "",'name_item' => "",'desc_pn' => "",'bpid' => ""));
        return redirect()->to('/admin'); 
    }
}
