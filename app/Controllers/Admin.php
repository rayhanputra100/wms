<?php

namespace App\Controllers;

use App\Models\TItems;
use App\Models\TReceh;
use App\Models\TOut;

class Admin extends BaseController
{
    protected $tItems;
    protected $tReceh;
    protected $tOut;

    public function __construct()
    {
        $this->tItems = new TItems();
        $this->tReceh = new TReceh();
        $this->tOut = new TOut();
    }

    public function index()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/admin', $data);
    }

    public function rackOthers()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/rack/rackOthers', $data);
    }

    public function rackV()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/rack/rackV', $data);
    }

    public function rackW()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/rack/rackW', $data);
    }

    public function rackX()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/rack/rackX', $data);
    }

    public function rackY()
    {
        $data = [
            'title' => 'List Item',
            'item' => $this->tItems->getItem(),
        ];
        return view('pages/rack/rackY', $data);
    }

    public function detail($id)
    {
        $data = [
            'title' => 'Detail Item',
            'item' => $this->tItems->getItem($id),
            'item2' => $this->tReceh->getItem(),
        ];
        return view('pages/detail', $data);
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Form Ubah Data Item',
            'validation' => \Config\Services::validation(),
            'item' => $this->tItems->getItem($id)

        ];

        return view('pages/form/formEdit', $data);
    }

    public function editReceh($id_receh)
    {
        $data = [
            'title' => 'Form Ubah Data Item',
            'validation' => \Config\Services::validation(),
            'item' => $this->tReceh->getItem($id_receh)

        ];

        return view('pages/form/formEditReceh', $data);
    }
    public function scan()
    {
        $data = [
            'title' => 'Form Scan Data Item',
            'validation' => \Config\Services::validation(),
            'item' => $this->tItems->getItem()

        ];

        return view('pages/form/formScan', $data);
    }

    public function delete($id)
    {
        $data['item'] = $this->tItems->getItem($id);
        $rack = $data['item']['rack'];
        $this->tItems->delete($id);

        session()->setFlashdata('pesan', 'Rack berhasil dihapus.');
        return redirect()->to('/rack' . $rack);
    }

    public function delReceh($id_receh)
    {
        $datItems['item'] = $this->tItems->getItem($id_receh);
        $datReceh['item'] = $this->tReceh->getItem($id_receh);
        $rack = $datItems['item']['rack'];

        $this->tOut->save([
            'id_data' => "",
            'id_receh' => $id_receh,
            'no_rfq' => $datReceh['item']['no_rfq_receh'],
            'no_wo' => $datReceh['item']['no_wo_receh'],
            'name_cust' => $datReceh['item']['name_cust_receh'],
            'code_qr' => $datReceh['item']['code_qr_receh'],
            'qty' => $datReceh['item']['qty_receh'],
            'locations' => $datReceh['item']['locations_receh'],
            'sub_locations' => $datReceh['item']['sub_locations_receh'],
            'rack' => $datReceh['item']['rack_receh'],
            'warehouse' => $datReceh['item']['warehouse_receh'],
            'no_tag' => $datReceh['item']['no_tag_receh'],
            'desc_pn' => $datReceh['item']['desc_pn_receh'],
            'name_item' => $datReceh['item']['name_item_receh'],
            'bpid' => $datReceh['item']['bpid_receh'],
            'no_sdf' => $datReceh['item']['no_sdf_receh'],
            'lot_del' => $datReceh['item']['lot_del_receh'],
        ]);
        $rack = $datReceh['item']['rack_receh'];

        $this->tItems->update($datReceh['item']['id_data'], array('name_item_receh' => NULL));

        $this->tReceh->delete($id_receh);
        return redirect()->to('/rack' . $rack);
    }

    public function reset($id)
    {
        $data['item'] = $this->tItems->getItem($id);
        $datReceh = null;
        $receh['item'] = $this->tReceh->getItem();
        foreach($receh['item'] as $v) {
            
            $datReceh = isset($v['name_item_receh']) ? $v['name_item_receh'] : null;
        }
        
        $rack = $data['item']['rack'];
        $this->tOut->save([
            'id_data' => $id,
            'id_receh' => "",
            'no_rfq' => $data['item']['no_rfq'],
            'no_wo' => $data['item']['no_wo'],
            'name_cust' => $data['item']['name_cust'],
            'code_qr' => $data['item']['code_qr'],
            'qty' => $data['item']['qty'],
            'locations' => $data['item']['locations'],
            'sub_locations' => $data['item']['sub_locations'],
            'rack' => $data['item']['rack'],
            'warehouse' => $data['item']['warehouse'],
            'no_tag' => $data['item']['no_tag'],
            'desc_pn' => $data['item']['desc_pn'],
            'name_item' => $data['item']['name_item'],
            'bpid' => $data['item']['bpid'],
            'no_sdf' => $data['item']['no_sdf'],
            'lot_del' => $data['item']['lot_del'],
        ]);


        if ($datReceh == !null) {
            $this->tItems->update($id, array('no_rfq' => NULL, 'no_sdf' => NULL, 'lot_del' => NULL, 'no_wo' => NULL, 'name_cust' => NULL, 'code_qr' => NULL, 'qty' => 0, 'warehouse' => NULL, 'no_tag' => NULL, 'name_item' => NULL, 'name_item_receh' => $datReceh, 'desc_pn' => NULL, 'bpid' => NULL));
        } else {
            $this->tItems->update($id, array('no_rfq' => NULL, 'no_sdf' => NULL, 'lot_del' => NULL, 'no_wo' => NULL, 'name_cust' => NULL, 'code_qr' => NULL, 'qty' => 0, 'warehouse' => NULL, 'no_tag' => NULL, 'name_item' => NULL, 'name_item_receh' => NULL, 'desc_pn' => NULL, 'bpid' => NULL));
        }
        return redirect()->to('/rack' . $rack);
    }
}
