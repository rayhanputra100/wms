<?php

namespace App\Models;

use CodeIgniter\Model;

class TReceh extends Model
{
    // protected $table            = 'whfg_racking_receh';
    protected $table            = 'TReceh';
    protected $allowedFields    = ['id_data','no_rfq_receh', 'no_wo_receh', 'name_cust_receh','no_sdf_receh','lot_del_receh', 'qty_receh', 'code_qr_receh', 'locations_receh','sub_locations_receh', 'rack_receh','warehouse_receh','no_tag_receh','name_item_receh','desc_pn_receh','bpid_receh'];
    protected $useTimestamps = true;
    protected $primaryKey = 'id_receh';

    public function getItem($id_receh = false)
    {
        if($id_receh == false){
            return $this->findAll();
        }

        return $this->where(['id_receh' => $id_receh])->first();
    }


}
