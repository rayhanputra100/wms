<?php

namespace App\Models;

use CodeIgniter\Model;

class TData extends Model
{
    // protected $table            = 'whfg_racking';
    protected $table            = 'TData';
    protected $allowedFields    = ['no_rfq', 'no_wo', 'name_cust', 'qty', 'code_qr','warehouse','no_tag','name_item','desc_pn','bpid'];
    protected $useTimestamps = true;

    public function getData($id = false)
    {
        if($id == false){
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }


}
