<?php

namespace App\Models;

use CodeIgniter\Model;

class TBackup extends Model
{
    // protected $table            = 'whfg_racking_backup';
    protected $table            = 'TBackup';
    protected $allowedFields    = ['id','no_rfq', 'no_wo', 'name_cust', 'qty', 'code_qr', 'locations','sub_locations', 'rack','warehouse','no_tag','name_item','desc_pn','bpid'];
    protected $useTimestamps = true;

    public function getItem($id = false)
    {
        if($id == false){
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }


}
