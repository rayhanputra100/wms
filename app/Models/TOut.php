<?php

namespace App\Models;

use CodeIgniter\Model;

class TOut extends Model
{
    // protected $table            = 'whfg_racking_log';
    protected $table            = 'TOut';
    protected $allowedFields    = ['id_receh','id_data','no_rfq', 'no_wo', 'name_cust','no_sdf','lot_del', 'qty', 'code_qr', 'locations','sub_locations', 'rack','warehouse','no_tag','name_item','desc_pn','bpid'];
    protected $useTimestamps = true;
    protected $primaryKey = 'id_out';

    public function getItem($id_out = false)
    {
        if($id_out == false){
            return $this->findAll();
        }

        return $this->where(['id_out' => $id_out])->first();
    }


}
