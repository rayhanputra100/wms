<?php

namespace App\Models;

use CodeIgniter\Model;

class TTemp extends Model
{
    // protected $table            = 'whfg_temp_rfid';
    protected $table            = 'TTemp';
    protected $allowedFields    = ['locations','sub_locations', 'rack','barcode', 'status'];
    protected $useTimestamps = true;

    public function getItem($id = false)
    {
        if($id == false){
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }


}
