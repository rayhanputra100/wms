<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/homeV2', 'Home::landingPage');
$routes->get('/admin', 'Admin::index');
$routes->get('/rackV', 'Admin::rackV');
$routes->get('/rackW', 'Admin::rackW');
$routes->get('/rackX', 'Admin::rackX');
$routes->get('/rackY', 'Admin::rackY');
$routes->get('/rackOthers', 'Admin::rackOthers');
$routes->get('/fetch', 'Form::fetch');
$routes->get('/fetch2', 'Form::fetch2');
$routes->get('/coba', 'Form::coba');
$routes->get('/checkItems', 'Form::check');
$routes->get('/export', 'ListData::exportExcel');
$routes->get('/exportReceh', 'ListDataReceh::exportExcel');

$routes->get('/admin/create', 'Form::create');
$routes->post('/admin/save', 'Form::save');

$routes->get('/admin/scan', 'Admin::scan');
$routes->post('/admin/scandat', 'Form::scanning');

$routes->get('/delete/(:any)', 'Admin::delete/$1');

$routes->get('/admin/delreceh/(:any)', 'Admin::delReceh/$1');
$routes->get('/admin/detail/(:any)', 'Admin::detail/$1');
$routes->get('/admin/tambah/(:any)', 'Form::tambahForm/$1');
$routes->post('/admin/tambah', 'Form::tambahDataReceh');

$routes->get('/admin/reset/(:any)', 'Admin::reset/$1');
$routes->get('/admin/(:any)', 'Admin::edit/$1');
$routes->get('/receh/(:any)', 'Admin::editReceh/$1');
$routes->post('/admin/update/(:any)', 'Form::update/$1');
$routes->post('/admin/updatereceh/(:any)', 'Form::updateDataReceh/$1');

$routes->get('/listdata', 'ListData::index');
$routes->get('/listdata/(:any)', 'ListData::edit/$1');
$routes->get('/listreset/(:any)', 'Form::reset/$1');
$routes->post('/bypass/(:any)', 'Form::bypass/$1');
$routes->post('/listdata/update/(:any)', 'Form::updateData/$1');
$routes->get('/json/(:any)', 'ListData::json/$1');
$routes->get('/gqr/(:any)', 'ListData::qr/$1');

$routes->get('/listdatareceh', 'ListDataReceh::index');
$routes->get('/listdatatemp', 'ListDataTemp::index');
// QR routes
$routes->get('/adminqr', 'FormGQR::index');
$routes->get('/adminqr/create', 'FormGQR::create');
$routes->post('/adminqr/save', 'FormGQR::save');
$routes->get('/adminqr/detail/(:any)', 'FormGQR::detail/$1');
$routes->get('/adminqr/json/(:any)', 'FormGQR::json/$1');
$routes->get('/adminqr/qr/(:any)', 'FormGQR::qr/$1');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
